from machine import Pin, PWM, Timer, UART

import utime

bt_uart = UART(0, 9600)
#bt_TXD = Pin(1, Pin.OUT)
l_cwPin = 2 #Pin(2, Pin.OUT)
l_acwPin = 3 #Pin(3, Pin.OUT)
r_cwPin = 4 #Pin(4, Pin.OUT)
r_acwPin = 5 #Pin(5, Pin.OUT)
back_trigger = Pin(6, Pin.OUT)
back_echo = Pin(7, Pin.IN)
#Pin(6, Pin.OUT)
#Pin(7, Pin.OUT)
#Pin(8, Pin.OUT)
#Pin(9, Pin.OUT)
#bt_state = Pin(10, Pin.IN)
#bt_RXD = Pin(11, Pin.IN)
#bt_TXD = Pin(12, Pin.OUT)
#bt_en = Pin(13, Pin.OUT)
bottom_back_trigger = Pin(14, Pin.OUT)
bottom_back_echo = Pin(15, Pin.IN)

#l_cwPin = 28 #Pin(28, Pin.OUT)
#l_acwPin = 27 #Pin(27, Pin.OUT)
#r_cwPin = 26 #Pin(26, Pin.OUT)
#r_acwPin = 22 #Pin(22, Pin.OUT)
l_pwmPIN = 21 #Pin(21, Pin.OUT)
r_pwmPIN = 20 #Pin(20, Pin.OUT)
front_echo = Pin(19, Pin.IN)
front_trigger = Pin(18, Pin.OUT)
bottom_front_echo = Pin(17, Pin.IN)
bottom_front_trigger = Pin(16, Pin.OUT)

FWD = 1
BWD = -1
STOP = 0

LEFT = 1
RIGHT = -1

def motorMove(speed,direction,speedGP,cwGP,acwGP):
  if speed > 100: speed=100
  if speed < 0: speed=0
  Speed = PWM(Pin(speedGP))
  Speed.freq(50)
  cw = Pin(cwGP, Pin.OUT)
  acw = Pin(acwGP, Pin.OUT)
  Speed.duty_u16(int(speed/100*65536))
  if direction < 0:
      cw.value(0)
      acw.value(1)
  if direction == 0:
      cw.value(0)
      acw.value(0)
  if direction > 0:
      cw.value(1)
      acw.value(0)

def move(speed, direction):
    motorMove(speed, direction, l_pwmPIN, l_cwPin, l_acwPin)
    motorMove(speed, direction, r_pwmPIN, r_cwPin, r_acwPin)

def stop():
    move(0, STOP)

def turn(speed, direction):
    if (direction == LEFT):
        motorMove(speed, -1, l_pwmPIN, l_cwPin, l_acwPin)
        motorMove(speed, 1, r_pwmPIN, r_cwPin, r_acwPin)
    elif (direction == RIGHT):
        motorMove(speed, 1, l_pwmPIN, l_cwPin, l_acwPin)
        motorMove(speed, -1, r_pwmPIN, r_cwPin, r_acwPin)
    else:
        stop()

def get_distance(trigger, echo):
   trigger.low()
   utime.sleep_us(2)
   trigger.high()
   utime.sleep_us(5)
   trigger.low()
   while echo.value() == 0:
       signaloff = utime.ticks_us()
   while echo.value() == 1:
       signalon = utime.ticks_us()
   timepassed = signalon - signaloff
   distance = (timepassed * 0.0343) / 2
   #print("The distance from object is ",distance,"cm")
   if (distance > 1000):
       distance = 0
   return distance

# main program

def prevent_front(t):
    halt_front = False
    
    dist_front = get_distance(front_trigger, front_echo)
    dist_bottom_front = get_distance(bottom_front_trigger, bottom_front_echo)
    
    if (dist_front < 10):
        halt_front = True
    
    if (dist_bottom_front > 5):
        halt_front = True

    if (halt_front):
        stop()
        move(50, BWD)
        utime.sleep(0.5)
        stop()
        
def prevent_back(t):
    halt_back = False
    
    dist_back = get_distance(back_trigger, back_echo)
    dist_bottom_back = get_distance(bottom_back_trigger, bottom_back_echo)
    
    if (dist_back < 10):
        halt_back = True
    
    if (dist_bottom_back > 5):
        halt_back = True

    if (halt_back):
        stop()
        move(50, FWD)
        utime.sleep(0.5)
        stop()
        
Timer().init(freq=5, mode=Timer.PERIODIC, callback=prevent_front)
Timer().init(freq=5, mode=Timer.PERIODIC, callback=prevent_back)

def send_AT(uart, cmd):
    uart.write(cmd+"\r\n")
    result = ''
    while True:
        if uart.any() > 0:
            result = uart.read()
            break
    return result

print(send_AT(bt_uart, "AT+NAME=ROBOT"))
print(send_AT(bt_uart, 'AT+PSWD="6485"'))

current_direction = STOP

while True:
    if bt_uart.any() > 0:
        data = bt_uart.read()
        if data == b'fwd':
            move(50, FWD)
        elif data == b'bwd':
            move(50, BWD)
        elif data == b'stop':
            stop()
        elif data == b'left':
            turn(50, LEFT)
            #utime.sleep(0.5)
            #stop()
        elif data == b'right':
            turn(50, RIGHT)
            #utime.sleep(0.5)
            #stop()
        else:
            print(data)